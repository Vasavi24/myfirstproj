package assessment;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/* Write a Java program to reverse elements in an array list. */
public class Question1 {
    public static void main(String[] args) {
        Question1 q1 = new Question1();
        List<String> output = q1.reverseList();
    }

    public List<String> reverseList() {
        // The list given below is the input
        String[] colours = {"Red", "Green", "Blue", "Yellow", "Brown", "Grey", "Violet", "Purple", "Orange", "Black"};
        ArrayList<String> input = new ArrayList<>(List.of(colours));

        // Reverse the list
        Collections.reverse(input);

        // Return reversed list
        return input;
    }
}